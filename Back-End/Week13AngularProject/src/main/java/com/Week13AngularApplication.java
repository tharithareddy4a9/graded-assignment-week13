package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEurkaClient

public class Week13AngularApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week13AngularApplication.class, args);
		System.err.println("Server Running on Port Number 8881");
	}

}
